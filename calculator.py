def calculator():
    calculation = 'y'
    while calculation.lower() == 'y':
        try:
            number_1 = float(input('Enter your first number: '))
        except ValueError:
            print('Please enter a number. Try again')
            continue
        operation = input('Please enter the operation you want to perform: +, -, *, /, ex: ')
        try:
            number_2 = float(input('Enter your second number: '))
        except ValueError:
            print('Please enter a number. Try again')
            continue
        if operation == '+':
            print('{} + {} = '.format(number_1, number_2), number_1 + number_2)
        elif operation == '-':
            print('{} - {} = '.format(number_1, number_2), number_1 - number_2)
        elif operation == '*':
            print('{} * {} = '.format(number_1, number_2), number_1 * number_2)
        elif operation == '/' and number_2 != 0:
            print('{} / {} = '.format(number_1, number_2), number_1 / number_2)
        elif operation == '/' and number_2 == 0:
            print('Cannot be divided by zero')
        elif operation == 'ex':
            print('{} ^ {} = '.format(number_1, number_2), number_1 ** number_2)
        else:
            print('Please enter a valid operator. Try again')
            continue;
        calculation = input('Do you want to calculate again? Enter Y for continue, another character to exit: ')